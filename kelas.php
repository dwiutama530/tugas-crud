<html>
<title>CRUD</title>
<head>
<!-- pencantuman link css yang digunakan -->
<link rel="stylesheet" href="style/materialize.min.css" />
</head>
<body>
<nav>
<!-- container untuk navigasi -->
		<div class="nav-wrapper teal">
        <div class="container">
          <a href="utama.php" class="brand-logo center white-text">CRUD</a>
        </div>
        </div>
</nav>
<!-- pengaturan style conten judul -->
<div class="container" style="margin-top:8%">
	<div class="row">
		<div class="col-md-8 col-md-offset-2"> 
			<p>
				<center>
					<h5 class="blue white-text">Data Kelas</h5><hr>
				</center>
			</p>
			<br>
			<p>
			<!-- pembuatan tabel dengan menggunakan class css -->
				<a class="btn btn-primary" href="tambah_kelas.php">Tambah</a>
			</p>
			<table class="table table-bordered">
				<tr>
					<th>
						No 
					</th>
					<th>
						ID Kelas 
					</th>
					<th>
						Nama Kelas
					</th>
					<th>
						Prodi
					</th>
					<th>
						Fakultas
					</th>
				</tr>
				<!-- sql mengkoneksi ke database -->
					<?php
						include"koneksi.php";
						$no = 1;
						$data = mysqli_query ($koneksi, " select 
																id_kelas,
																nama_kelas,
																prodi,
																fakultas
														  from 
														  kelas
														  order by id_kelas DESC");
						while ($row = mysqli_fetch_array ($data))
						{
					?>
					<!-- pemanggialan variabel dan menampilkan -->
				<tr>
					<td>
						<?php echo $no++; ?>
					</td>
					<td>
						<?php echo $row['id_kelas']; ?>
					</td>
					<td>
						<?php echo $row['nama_kelas']; ?>
					</td>
					<td>
						<?php echo $row['prodi']; ?>
					</td>
					<td>
						<?php echo $row['fakultas']; ?>
					</td>
					<!-- button hapus dan edit yang mengarahkan kelas yang telah dicantumkan -->
					<td>
						<a class="btn green" href="edit_kelas.php?id=<?php echo $row['id_kelas']; ?>">Edit</a> 
						<a class="btn red" href="hapus_k.php?id=<?php echo $row['id_kelas']; ?>">Hapus</a>
					</td>
				</tr>
				<?php
					}
				?>
			</table>
		</div>
	</div>
</div>
</body>
</html>