<html>
<title>CRUD</title>
<!-- pencantuman link css yang digunakan -->
<head>
    <link rel="stylesheet" href="style/materialize.min.css" />
</head>

<body>
    <nav>
        <div class="nav-wrapper teal">
            <div class="container">
                <a href="utama.php" class="brand-logo center white-text">CRUD - CREAT READ UPDATE DELETE</a>
            </div>
        </div>
    </nav>
    <?php
	include"koneksi.php";
	$no = 1;
	$data = mysqli_query ($koneksi, " select 
											id_kelas,
											nama_kelas,
											prodi,
											fakultas
									  from 
									  kelas 
									  where id_kelas = $_GET[id]");
	$row = mysqli_fetch_array ($data);
	
?>
    <div class="container" style="margin-top:8%">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <p>
                    <center>
                        <h5>Edit Data <?= $row['nama_kelas'] ; ?></h5>
                        <hr>
                    </center>
                </p>
                <br>

                <form role="form" method="post" action="update_kelas.php">
                    <div class="form-group">
                        <label>Nama Kelas</label>
                        <input type="hidden" name="id_kelas" value="<?php echo $row['id_kelas'] ; ?>">
                        <input class="form-control" name="nama_kelas" value="<?php echo $row['nama_kelas'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label>Prodi</label>
                        <input class="form-control" name="prodi" value="<?php echo $row['prodi'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label>Fakultas</label>
                        <input class="form-control" name="fakultas" value="<?php echo $row['fakultas'] ; ?>">
                    </div>
                    <button type="submit" class="btn green">Perbarui</button>
                    <a href="kelas.php" class="btn red" style="margin-right:1%;">Batal</a>
                </form>
            </div>
        </div>
    </div>
    <script src="style/materialize.min.js"></script>
</body>

</html>