<html>
<title>CRUD</title>
<!-- pencantuman link css yang digunakan -->
<head>
    <link rel="stylesheet" href="style/materialize.min.css" />
</head>
<body>
<!-- container untuk navigasi -->
<nav>
        <div class="nav-wrapper teal">
        <div class="container">
          <a href="utama.php" class="brand-logo center white-text">CRUD</a>
        </div>
        </div>
</nav>
    <!-- pengaturan style tabel -->
    <div class="container" style="margin-top:8%">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <p>
                    <center>
                        <h5>Tambah Data Dosen</h5><hr>
                    </center>
                </p>
                <br>
                <!-- pembuatan form dengan menggunakan class css -->
                <form role="form" method="post" action="input.php">
                    <div class="form-group">
                        <label>Foto Dosen</label>
                        <input class="file-control" type ="file"name="foto_dosen">
                    </div>
                    <div class="form-group">
                        <label>NIP</label>
                        <input class="form-control" name="nip_dosen">
                    </div>
                    
                    <div class="form-group">
                        <label>Nama Dosen</label>
                        <input class="form-control" name="nama_dosen">
                    </div>
                    <div class="form-group">
                        <label>Prodi</label>
                        <input class="form-control" name="prodi">
                    </div>
                    <div class="form-group">
                        <label>Fakultas</label>
                        <input class="form-control" name="fakultas">
                    </div>
                    <!-- submit yang diarahkan ke jadwal.php -->
                    <button type="submit" class="btn green">Simpan</button>
                    <a href="dosen.php" class="btn red" style="margin-right:1%;">Batal</a>
                </form>
            </div>
        </div>
        <p>
    </div>
      <!-- pemanggilan json -->
    <script src="style/materialize.min.js"></script>
</body>

</html>